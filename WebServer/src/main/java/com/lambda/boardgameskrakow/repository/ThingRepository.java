package com.lambda.boardgameskrakow.repository;

import com.lambda.boardgameskrakow.model.entity.Thing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThingRepository extends JpaRepository<Thing, Integer> {

    Thing findByName(String name);
}