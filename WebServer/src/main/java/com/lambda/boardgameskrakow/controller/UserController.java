package com.lambda.boardgameskrakow.controller;


import com.lambda.boardgameskrakow.exception.ResourceNotFoundException;
import com.lambda.boardgameskrakow.model.dto.ThingOutputDto;
import com.lambda.boardgameskrakow.model.dto.UserDto;
import com.lambda.boardgameskrakow.model.entity.User;
import com.lambda.boardgameskrakow.service.ThingService;
import com.lambda.boardgameskrakow.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

    private final UserService userService;
    private final ThingService thingService;
    private ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService, ThingService thingService) {
        this(userService, thingService, new ModelMapper());
    }

    public UserController(
            UserService userService,
            ThingService thingService,
            ModelMapper modelMapper
    ) {
        this.userService = userService;
        this.thingService = thingService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("/signup")
    public UserDto createUser(@RequestBody UserDto userDto) {
        UserDto newUserDto = modelMapper.map(userService.saveUser(modelMapper.map(userDto, User.class)), UserDto.class);

        return newUserDto;
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public UserDto getUserById(@PathVariable(value = "id") int id) throws ResourceNotFoundException {
        if (!userService.findUserById(id)) {
            throw new ResourceNotFoundException("User", "id", id);
        }

        UserDto userDto = modelMapper.map(userService.getUserById(id), UserDto.class);

        return userDto;
    }

    @GetMapping()
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserDto> getAllUsers() {
        List<User> users = userService.findAll();
        List<UserDto> usersDto = new ArrayList<>();

        users.forEach(x -> {
                    UserDto userDto = modelMapper.map(x, UserDto.class);
                    usersDto.add(userDto);
                }
        );

        return usersDto;
    }

    @GetMapping("/things")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<ThingOutputDto> getAllUserThings() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails) principal).getUsername();

//        List<Thing> things = userService.findByUsername(username).getThings();
        List<ThingOutputDto> thingOutputsDto = new ArrayList<>();

//        things.forEach(x -> {
//                    ThingOutputDto thingOutputDto = modelMapper.map(x, ThingOutputDto.class);
//                    thingOutputsDto.add(thingOutputDto);
//                }
//        );

        return thingOutputsDto;
    }
}
