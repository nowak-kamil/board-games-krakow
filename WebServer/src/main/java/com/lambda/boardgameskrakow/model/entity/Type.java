package com.lambda.boardgameskrakow.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "type")
public class Type {

    @Id
    @Column(name = "type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "type_name", length = 100)
    private String name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "type_thing", joinColumns = {
            @JoinColumn(name = "thing_id")}, inverseJoinColumns = {
            @JoinColumn(name = "type_id")})
    private Set<Thing> things;
}
