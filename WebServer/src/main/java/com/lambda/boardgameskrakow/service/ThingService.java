package com.lambda.boardgameskrakow.service;

import com.lambda.boardgameskrakow.model.entity.Thing;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ThingService {

    boolean findThingById(int id);

    Thing getThingById(int id);

    Thing saveThing(Thing thing);

    List<Thing> findAll();

    void delete(int id);

    Thing findByThingname(String thingname);

    Thing findById(int id);

}
