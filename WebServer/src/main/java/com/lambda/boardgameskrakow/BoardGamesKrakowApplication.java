package com.lambda.boardgameskrakow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.lambda.boardgameskrakow"})
@EntityScan("com.lambda.boardgameskrakow.model")
@EnableJpaRepositories("com.lambda.boardgameskrakow.repository")
public class BoardGamesKrakowApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoardGamesKrakowApplication.class, args);
	}

}
